package handler

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/billing/go-insertdb/model"
)

// Match :
type Match struct {
	ListTask *model.TaskInsertDB
}

// CopyToDatabase :
func (p *Match) CopyToDatabase(dataType string) (Process, error) {
	var hProcess Process
	var modelMatch model.Match

	inputFile := p.ListTask.PathInput + p.ListTask.FilenameInput
	tmpFile := inputFile + ".tmp"

	// Opening Input File, that is Unduplicate File
	fmt.Printf("  > [ Opening Input File ] : " + inputFile + " => ")
	fileInput, err := os.OpenFile(inputFile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	fmt.Println("Success")

	// Making Temporary File, Used For Copying Data to Database
	// Temporary File needed because the original File didn't have Batch ID
	fileTemp, err := os.Create(tmpFile)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}

	// Creating Temporary File buffered Writer
	writerTemp := bufio.NewWriter(fileTemp)

	fmt.Printf("  > [ Writing Temp File ] : " + tmpFile + " => ")
	sc := bufio.NewScanner(fileInput)
	for sc.Scan() {
		// Adding Batch ID to Record & Writing To Temporary File
		fmt.Fprintln(writerTemp, p.ListTask.BatchID+"|"+sc.Text()+"|"+p.ListTask.MoMt)
	}
	if err := sc.Err(); err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	// Flush buffered data after writing
	err = writerTemp.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	// Close File Temporary I/O
	fileTemp.Close()
	// Close File Input I/O
	fileInput.Close()

	fmt.Println("Success")

	currentModel := model.StructCopyToTable{Filename: tmpFile, Periode: p.ListTask.Periode}
	fmt.Printf("  > [ Doing Copy Data To Database ] : ")
	// Calling API For Copying Data To Database
	err = modelMatch.CopyToDatabase(dataType, p.ListTask.FileType, &currentModel)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	fmt.Println("Success")

	// Remove Temporary File From Storage
	os.Remove(tmpFile)

	hProcess.ProcessStatusID = "7"

	return hProcess, nil
}

// InsertData :
func (p *Match) InsertData() (Process, string) {
	var hProcess Process

	// Opening File CDR
	inputFile := p.ListTask.PathInput + p.ListTask.FilenameInput

	fmt.Printf("  > [ Opening Input File ] : " + inputFile + " => ")
	fileCDR, err := os.OpenFile(inputFile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err.Error()
	}
	fmt.Println("Success")
	defer fileCDR.Close()

	// Processing insert record of Input File
	fmt.Printf("  > [ Doing Insert Record ] : ")
	sc := bufio.NewScanner(fileCDR)
	countInsert := 0
	for sc.Scan() {
		countInsert++
		columns := strings.Split(sc.Text(), "|")
		modelTAPIN := model.TAPIN{
			p.ListTask.BatchID,
			GetFirstArg(strconv.Atoi(columns[0])).(int),
			GetFirstArg(strconv.Atoi(columns[1])).(int),
			GetFirstArg(strconv.Atoi(columns[2])).(int),
			GetFirstArg(strconv.Atoi(columns[3])).(int),
			GetFirstArg(strconv.Atoi(columns[4])).(int),
			columns[5],
			GetFirstArg(strconv.Atoi(columns[6])).(int),
			GetFirstArg(strconv.Atoi(columns[7])).(int),
			GetFirstArg(strconv.Atoi(columns[8])).(int),
			GetFirstArg(strconv.Atoi(columns[9])).(int),
			columns[10],
			columns[11],
			columns[12],
			columns[13],
			columns[14],
			GetFirstArg(strconv.Atoi(columns[15])).(int),
			p.ListTask.MoMt,
		}

		modelTAPIN.InsertToDatabase(p.ListTask.Periode)
	}
	if err := sc.Err(); err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err.Error()
	}
	fmt.Println("Finished, total insert =>", countInsert)

	hProcess.ProcessStatusID = "7"

	return hProcess, ""
}
