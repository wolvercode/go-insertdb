package handler

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/billing/go-insertdb/model"
)

// TAPINOnly :
type TAPINOnly struct {
	ListTask *model.TaskInsertDB
}

// CopyToDatabase :
func (p *TAPINOnly) CopyToDatabase(dataType string) (Process, error) {
	var hProcess Process
	var modelTapinOnly model.TAPINOnly

	inputFile := p.ListTask.PathInput + p.ListTask.FilenameInput
	tmpFile := inputFile + ".tmp"

	// Opening Input File, that is Unduplicate File
	fmt.Printf("  > [ Opening Input File ] : " + inputFile + " => ")
	fileInput, err := os.OpenFile(inputFile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	fmt.Println("Success")

	// Making Temporary File, Used For Copying Data to Database
	// Temporary File needed because the original File didn't have Batch ID
	fileTemp, err := os.Create(tmpFile)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}

	// Creating Temporary File buffered Writer
	writerTemp := bufio.NewWriter(fileTemp)

	fmt.Printf("  > [ Writing Temp File ] : " + tmpFile + " => ")
	sc := bufio.NewScanner(fileInput)
	for sc.Scan() {
		// Adding Batch ID to Record & Writing To Temporary File
		fmt.Fprintln(writerTemp, p.ListTask.BatchID+","+sc.Text()+","+p.ListTask.MoMt)
	}
	if err := sc.Err(); err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	// Flush buffered data after writing
	err = writerTemp.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	// Close File Temporary I/O
	fileTemp.Close()
	// Close File Input I/O
	fileInput.Close()

	fmt.Println("Success")

	currentModel := model.StructCopyToTable{Filename: tmpFile, Periode: p.ListTask.Periode}
	fmt.Printf("  > [ Doing Copy Data To Database ] : ")
	// Calling API For Copying Data To Database
	err = modelTapinOnly.CopyToDatabase(dataType, p.ListTask.FileType, &currentModel)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err
	}
	fmt.Println("Success")

	// Remove Temporary File From Storage
	os.Remove(tmpFile)

	hProcess.ProcessStatusID = "7"

	return hProcess, nil
}

// InsertData :
func (p *TAPINOnly) InsertData() (Process, string) {
	var hProcess Process

	// Opening File CDR
	inputFile := p.ListTask.PathInput + p.ListTask.FilenameInput

	fmt.Printf("  > [ Opening Input File ] : " + inputFile + " => ")
	fileCDR, err := os.OpenFile(inputFile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err.Error()
	}
	fmt.Println("Success")
	defer fileCDR.Close()

	// Processing insert record of Input File
	fmt.Printf("  > [ Doing Insert Record ] : ")
	sc := bufio.NewScanner(fileCDR)
	countInsert := 0
	for sc.Scan() {
		countInsert++
		columns := strings.Split(strings.Replace(sc.Text(), "\"", "", -1), ",")
		modelTAPINOnly := model.TAPINOnly{
			p.ListTask.BatchID,
			columns[0],
			columns[1],
			columns[2],
			columns[3],
			columns[4],
			columns[5],
			columns[6],
			columns[7],
			columns[8],
			columns[9],
			columns[10],
			columns[11],
			columns[12],
			columns[13],
			columns[14],
			columns[15],
			columns[16],
			columns[17],
			columns[18],
			columns[19],
			columns[20],
			columns[21],
			columns[22],
			columns[23],
			columns[24],
			columns[25],
			columns[26],
			columns[27],
			columns[28],
			columns[29],
			columns[30],
			columns[31],
			columns[32],
			columns[33],
			columns[34],
			columns[35],
			columns[36],
			columns[37],
			p.ListTask.MoMt,
		}

		modelTAPINOnly.InsertToDatabase(p.ListTask.Periode)
	}
	if err := sc.Err(); err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID = "5"
		return hProcess, err.Error()
	}
	fmt.Println("Finished, total insert =>", countInsert)

	hProcess.ProcessStatusID = "7"

	return hProcess, ""
}
