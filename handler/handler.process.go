package handler

import (
	"fmt"

	"bitbucket.org/billing/go-insertdb/model"
)

// Process :
type Process struct {
	ProcessStatusID string `json:"processstatus_id"`
}

var modelProcess model.ProcessInsertDB

// GetProcess :
func (p *Process) GetProcess() ([]model.ProcessInsertDB, error) {
	listProcess, err := modelProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return listProcess, nil
}

// CloseProcess :
func (p *Process) CloseProcess(processID, processStatusID, errorMessage string) error {
	fmt.Printf("  > [ Closing Process ] : ")
	err := modelProcess.CloseProcess(processID, processStatusID, errorMessage)
	if err != nil {
		fmt.Println("Error, ", err.Error())
		return err
	}
	fmt.Println("Success")
	return nil
}

// GetTask :
func (p *Process) GetTask() ([]model.TaskInsertDB, error) {
	listProcess, err := modelProcess.GetTask()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return listProcess, nil
}
