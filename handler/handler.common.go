package handler

import (
	"github.com/syndtr/goleveldb/leveldb"
)

// CommonHandler :
type CommonHandler struct {
	DBConn *leveldb.DB
}

// GetFirstArg :
func GetFirstArg(args ...interface{}) interface{} {
	return args[0]
}
