package main

import (
	"bitbucket.org/billing/go-insertdb/app"
	"bitbucket.org/billing/go-insertdb/flow"
)

func main() {
	for {
		app.Initialize()

		// flow.DoJobsOld()
		flow.DoJobs()

		app.Sleeping()
	}
}
