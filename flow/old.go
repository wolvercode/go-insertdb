package flow

// // DoJobsOld :
// func DoJobsOld() error {
// 	fmt.Printf("[ Getting Jobs From Database ] : ")
// 	handlerProcess := handler.Process{}
// 	listProcess, err := handlerProcess.GetProcess()
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		return err
// 	}

// 	if listProcess == nil {
// 		fmt.Println("No job found in database")
// 		return nil
// 	}

// 	fmt.Println(len(listProcess), "Jobs")

// 	for _, v := range listProcess {
// 		if v.ProcessTypeID == "2-b" {
// 			switch v.DataTypeID {
// 			case 1:
// 				fmt.Println("[ Processing Data OCS Postpaid ] : { Process ID :", v.ProcessID, "}")
// 				modelProcess := model.ProcessInsertDB{}
// 				modelProcess.UpdateStatus(v.ProcessID, "4")

// 				err := modelProcess.CreateTableIfNotExists(v.Periode, "OCS", v.PostPre)
// 				if err != nil {
// 					continue
// 				}

// 				handlerOCS := handler.OCS{ListProcess: &v}

// 				// processResult, errMsg := handlerOCS.InsertData()
// 				processResult, errMsg := handlerOCS.CopyToDatabase(v.PostPre)
// 				processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)
// 			case 2:
// 				fmt.Println("[ Processing Data TAPIN ] : { Process ID :", v.ProcessID, "}")
// 				modelProcess := model.ProcessInsertDB{}
// 				modelProcess.UpdateStatus(v.ProcessID, "4")

// 				err := modelProcess.CreateTableIfNotExists(v.Periode, "TAPIN", v.PostPre)
// 				if err != nil {
// 					continue
// 				}

// 				handlerTAPIN := handler.TAPIN{ListProcess: &v}

// 				// processResult, errMsg := handlerTAPIN.InsertData()
// 				processResult, errMsg := handlerTAPIN.CopyToDatabase(v.PostPre)
// 				processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)
// 			}
// 		} else if v.ProcessTypeID == "3-b" {
// 			fmt.Println("[ Processing Data Tapin Only ] : { Process ID :", v.ProcessID, "}")
// 			modelProcess := model.ProcessInsertDB{}
// 			modelProcess.UpdateStatus(v.ProcessID, "4")

// 			err := modelProcess.CreateTableIfNotExists(v.Periode, "TAPIN_ONLY", v.PostPre)
// 			if err != nil {
// 				continue
// 			}

// 			handlerTAPINOnly := handler.TAPINOnly{ListProcess: &v}

// 			// processResult, errMsg := handlerTAPINOnly.InsertData()
// 			processResult, errMsg := handlerTAPINOnly.CopyToDatabase(v.PostPre)
// 			processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)
// 		}
// 	}

// 	return nil
// }
