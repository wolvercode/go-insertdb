package flow

import (
	"fmt"

	"bitbucket.org/billing/go-insertdb/handler"
	"bitbucket.org/billing/go-insertdb/model"
)

// DoJobs :
func DoJobs() error {
	fmt.Printf("[ Getting Task From Database ] : ")
	handlerProcess := handler.Process{}
	listTask, err := handlerProcess.GetTask()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listTask == nil {
		fmt.Println("No task found in database")
		return nil
	}

	fmt.Println(len(listTask), "Tasks")

	for _, v := range listTask {
		fmt.Printf("[ Processing Data %s %s ] : { Process ID : %s }\n", v.DataType, v.FileType, v.ProcessID)
		switch v.DataType {
		case "TAPIN":
			switch v.FileType {
			case "DUP", "UNDUP":
				modelProcess := model.ProcessInsertDB{}
				modelProcess.UpdateStatusTask(v.ProcessID, "4", "")

				err := modelProcess.CreateTableIfNotExists(v.Periode, "TAPIN", v.FileType, v.PostPre)
				if err != nil {
					modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
					continue
				}

				handlerTAPIN := handler.TAPIN{ListTask: &v}

				// processResult, errMsg := handlerTAPIN.InsertData()
				_, err = handlerTAPIN.CopyToDatabase(v.PostPre)
				if err != nil {
					modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
					continue
				}
				modelProcess.UpdateStatusTask(v.ProcessID, "7", "")
				// processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)

			case "ONLY":
				modelProcess := model.ProcessInsertDB{}
				modelProcess.UpdateStatusTask(v.ProcessID, "4", "")

				err := modelProcess.CreateTableIfNotExists(v.Periode, "TAPIN", v.FileType, v.PostPre)
				if err != nil {
					modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
					continue
				}

				handlerTAPINOnly := handler.TAPINOnly{ListTask: &v}

				// processResult, errMsg := handlerTAPINOnly.InsertData()
				_, err = handlerTAPINOnly.CopyToDatabase(v.PostPre)
				if err != nil {
					modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
					continue
				}
				modelProcess.UpdateStatusTask(v.ProcessID, "7", "")
				// processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)

			case "MATCH":
				modelProcess := model.ProcessInsertDB{}
				modelProcess.UpdateStatusTask(v.ProcessID, "4", "")

				err := modelProcess.CreateTableIfNotExists(v.Periode, "TAPIN", v.FileType, v.PostPre)
				if err != nil {
					modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
					continue
				}

				handlerMatch := handler.Match{ListTask: &v}

				// processResult, errMsg := handlerTAPIN.InsertData()
				_, err = handlerMatch.CopyToDatabase(v.PostPre)
				if err != nil {
					modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
					continue
				}
				modelProcess.UpdateStatusTask(v.ProcessID, "7", "")
				// processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)
			}
		case "OCS":
			// switch v.FileType {
			// case "DUP", "UNDUP":
			modelProcess := model.ProcessInsertDB{}
			modelProcess.UpdateStatusTask(v.ProcessID, "4", "")

			err := modelProcess.CreateTableIfNotExists(v.Periode, "OCS", v.FileType, v.PostPre)
			if err != nil {
				modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
				continue
			}

			handlerOCS := handler.OCS{ListTask: &v}

			// processResult, errMsg := handlerOCS.InsertData()
			_, err = handlerOCS.CopyToDatabase(v.PostPre)
			if err != nil {
				modelProcess.UpdateStatusTask(v.ProcessID, "5", err.Error())
				continue
			}
			modelProcess.UpdateStatusTask(v.ProcessID, "7", "")
			// processResult.CloseProcess(v.ProcessID, processResult.ProcessStatusID, errMsg)
			// }
		}
	}

	return nil
}
