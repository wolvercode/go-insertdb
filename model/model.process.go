package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/billing/go-insertdb/app"
)

// ProcessInsertDB :
type ProcessInsertDB struct {
	ProcessID     string `json:"process_id"`
	BatchID       string `json:"batch_id"`
	ProcessTypeID string `json:"processtype_id"`
	Periode       string `json:"periode"`
	DataTypeID    int    `json:"data_type_id"`
	DayPeriode    string `json:"day"`
	MoMt          string `json:"mo_mt"`
	PostPre       string `json:"post_pre"`
	InputID       int    `json:"id_input"`
	PathInput     string `json:"path_input"`
	FilenameInput string `json:"filename_input"`
}

// GetProcess :
func (p *ProcessInsertDB) GetProcess() ([]ProcessInsertDB, error) {
	var listProcess []ProcessInsertDB

	response, err := http.Get(app.Appl.DBAPIURL + "process/insertdb/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// UpdateStatus :
func (p *ProcessInsertDB) UpdateStatus(processID, processStatusID string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/updateStatus/"+processID+"/"+processStatusID, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// CreateTableIfNotExists :
func (p *ProcessInsertDB) CreateTableIfNotExists(periode, dataType, fileType, postPre string) error {
	response, err := http.Get(app.Appl.DBAPIURL + "process/insertdb/createTable/" + periode + "/" + dataType + "/" + fileType + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		fmt.Println("Cannot Create New Table")
		return errors.New("Cannot Create Table")
	}

	defer io.Copy(ioutil.Discard, response.Body)

	return nil
}

// CloseProcess :
func (p *ProcessInsertDB) CloseProcess(processID, processStatusID, errorMessage string) error {
	jsonData := ClosingStruct{processID, processStatusID, errorMessage}
	jsonValue, _ := json.Marshal(jsonData)
	response, err := http.Post(app.Appl.DBAPIURL+"process/insertdb/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetTask :
func (p *ProcessInsertDB) GetTask() ([]TaskInsertDB, error) {
	var listProcess []TaskInsertDB

	response, err := http.Get(app.Appl.DBAPIURL + "process/insertdb/getTask")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// UpdateStatusTask :
func (p *ProcessInsertDB) UpdateStatusTask(processID, processStatusID, errMsg string) error {
	// jsonData := ClosingStruct{processID, processStatusID, errorMessage}
	jsonValue, _ := json.Marshal(errMsg)
	response, err := http.Post(app.Appl.DBAPIURL+"process/insertdb/updateStatus/"+processID+"/"+processStatusID, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
