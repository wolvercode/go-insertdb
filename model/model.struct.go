package model

// OCS : Struct of OCS Model
type OCS struct {
	BatchID       string `json:"batch_id"`
	Edtm          int    `json:"edtm"`
	Aparty        int    `json:"aparty"`
	Bparty        string `json:"bparty"`
	Imsi          int    `json:"imsi"`
	Callduration  int    `json:"callduration"`
	Roamingflag   int    `json:"roamingflag"`
	Camelroaming  int    `json:"camelroaming"`
	Accountdelta  string `json:"accountdelta"`
	Calltype      int    `json:"calltype"`
	ServiceFilter string `json:"service_filter"`
	TapCode       string `json:"tap_code"`
	CellID        string `json:"cell_id"`
	MoMt          string `json:"mo_mt"`
}

// TAPIN : Struct of TAPIN Model
type TAPIN struct {
	BatchID         string `json:"batch_id"`
	CamelServiceKey int    `json:"camel_service_key"`
	Imsi            int    `json:"imsi"`
	Msisdn          int    `json:"msisdn"`
	Othertelnum     int    `json:"othertelnum"`
	Calleddigit     int    `json:"calleddigit"`
	Partnerid       string `json:"partnerid"`
	LocalTime       int    `json:"local_time"`
	Starttime       int    `json:"starttime"`
	Dealtime        int    `json:"dealtime"`
	Duration        int    `json:"duration"`
	CdrType         string `json:"cdr_type"`
	Servicename     string `json:"servicename"`
	TapFilename     string `json:"tap filename"`
	TapFee          string `json:"tap fee"`
	UtcTime         string `json:"utc_time"`
	PlusUtcTime     int    `json:"plus_utc_time"`
	MoMt            string `json:"mo_mt"`
}

// TAPINOnly : Struct of TAPINOnly Model
type TAPINOnly struct {
	BatchID              string `json:"batch_id"`
	Eventsource          string `json:"event_source"`
	Eventtypeid          string `json:"event_type_id"`
	Starttime            string `json:"starttime"`
	Reserve1             string `json:"reserve1"`
	Reserve2             string `json:"reserve2"`
	Reserve3             string `json:"reserve3"`
	Reserve4             string `json:"reserve4"`
	Reserve5             string `json:"reserve5"`
	Reserve6             string `json:"reserve6"`
	Reserve7             string `json:"reserve7"`
	Reserve8             string `json:"reserve8"`
	Reserve9             string `json:"reserve9"`
	Reserve10            string `json:"reserve10"`
	Callednumber         string `json:"called_number"`
	Operatorid1          string `json:"operator_id1"`
	Calltype             string `json:"call_type"`
	Charge               string `json:"charge"`
	Callingnumber        string `json:"calling_number"`
	Reserve11            string `json:"reserve11"`
	Tapdecimalplaces     string `json:"tapdecimalplaces"`
	Dialednumber         string `json:"dialed_number"`
	Duration             string `json:"duration"`
	Msisdn               string `json:"msisdn"`
	Sessionid            string `json:"session_id"`
	Callforwardindicator string `json:"call_forward_indicator"`
	Roamingtypeindicator string `json:"roaming_type_indicator"`
	Operatorid2          string `json:"operator_id2"`
	Localtimestamp       string `json:"localtimestamp"`
	Reserve12            string `json:"reserve12"`
	Reserve13            string `json:"reserve13"`
	Reserve14            string `json:"reserve14"`
	Reserve15            string `json:"reserve15"`
	Utctimeoffset        string `json:"utc_time_offset"`
	Reserve16            string `json:"reserve16"`
	Reserve17            string `json:"reserve17"`
	Reserve18            string `json:"reserve18"`
	Reserve19            string `json:"reserve19"`
	Tapfilename          string `json:"tapfilename"`
	MoMt                 string `json:"mo_mt"`
}

// StructCopyToTable :
type StructCopyToTable struct {
	Filename string `json:"filename"`
	Periode  string `json:"periode"`
}

// ClosingStruct :
type ClosingStruct struct {
	ProcessID       string `json:"process_id"`
	ProcessStatusID string `json:"process_status_id"`
	ErrorMessage    string `json:"error_message"`
}

// TaskInsertDB :
type TaskInsertDB struct {
	ProcessID     string `json:"process_id"`
	BatchID       string `json:"batch_id"`
	Periode       string `json:"periode"`
	DataType      string `json:"data_type"`
	PostPre       string `json:"post_pre"`
	MoMt          string `json:"mo_mt"`
	InputID       int    `json:"id_input"`
	FileType      string `json:"filetype"`
	PathInput     string `json:"path_input"`
	FilenameInput string `json:"filename_input"`
}

// Match : Struct of Match Model
type Match struct {
	BatchID         string `json:"batch_id"`
	CamelServiceKey int    `json:"camel_service_key"`
	Imsi            int    `json:"imsi"`
	Msisdn          int    `json:"msisdn"`
	Othertelnum     int    `json:"othertelnum"`
	Calleddigit     int    `json:"calleddigit"`
	Partnerid       string `json:"partnerid"`
	LocalTime       int    `json:"local_time"`
	Starttime       int    `json:"starttime"`
	Dealtime        int    `json:"dealtime"`
	Duration        int    `json:"duration"`
	CdrType         string `json:"cdr_type"`
	Servicename     string `json:"servicename"`
	TapFilename     string `json:"tap filename"`
	TapFee          string `json:"tap fee"`
	UtcTime         string `json:"utc_time"`
	PlusUtcTime     int    `json:"plus_utc_time"`
	Edtm            int    `json:"edtm"`
	Aparty          int    `json:"aparty"`
	Bparty          string `json:"bparty"`
	ImsiOCS         int    `json:"imsi_ocs"`
	Callduration    int    `json:"callduration"`
	Roamingflag     int    `json:"roamingflag"`
	Camelroaming    int    `json:"camelroaming"`
	Accountdelta    string `json:"accountdelta"`
	Calltype        int    `json:"calltype"`
	ServiceFilter   string `json:"service_filter"`
	TapCode         string `json:"tap_code"`
	CellID          string `json:"cell_id"`
	MoMt            string `json:"mo_mt"`
}
