package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"bitbucket.org/billing/go-insertdb/app"
)

// CopyToDatabase :
func (p *OCS) CopyToDatabase(dataType, fileType string, currentModel *StructCopyToTable) error {
	jsonValue, err := json.Marshal(currentModel)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	response, err := http.Post(app.Appl.DBAPIURL+"process/insertdb/copy/ocs/"+dataType+"/"+fileType, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// InsertToDatabase :
func (p *OCS) InsertToDatabase(periode string) error {
	jsonValue, _ := json.Marshal(p)
	response, err := http.Post(app.Appl.DBAPIURL+"process/insertdb/ocs/"+periode, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
